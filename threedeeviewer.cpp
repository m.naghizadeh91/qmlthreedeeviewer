#include "threedeeviewer.h"

#include <QQuickVTKRenderItem.h>
#include <QQuickVTKRenderWindow.h>

void ThreeDeeViewer::registerQML() {
  qmlRegisterType<ThreeDeeViewer>("ThreeDeeViewer", 1, 0, "Viewer");
}

ThreeDeeViewer::ThreeDeeViewer(QQuickItem *parent) : QQuickItem(parent) {
  m_renderWindow = new QQuickVTKRenderWindow(this);
  m_renderer = new QQuickVTKRenderItem(this);
  m_renderer->setRenderWindow(m_renderWindow);
}

QQuickVTKRenderItem *ThreeDeeViewer::renderer() const { return m_renderer; }

void ThreeDeeViewer::setRenderer(QQuickVTKRenderItem *renderer) {
  if (m_renderer == renderer) {
    return;
  }

  m_renderer->deleteLater();

  m_renderer = renderer;
  emit rendererChanged(m_renderer);
}

QQuickVTKRenderWindow *ThreeDeeViewer::renderWindow() const {
  return m_renderWindow;
}

void ThreeDeeViewer::setRenderWindow(QQuickVTKRenderWindow *renderWindow) {
  if (m_renderWindow == renderWindow) {
    return;
  }

  m_renderWindow = renderWindow;
  emit renderWindowChanged(m_renderWindow);
}

QColor ThreeDeeViewer::color() const { return m_color; }

void ThreeDeeViewer::setColor(QColor color) {
  if (color == m_color) {
    return;
  }

  m_color = color;
  emit colorChanged(m_color);
  double backgroundColor[3];
  backgroundColor[0] = color.redF();
  backgroundColor[1] = color.greenF();
  backgroundColor[2] = color.blueF();
  m_renderer->renderer()->SetBackground(backgroundColor);
}