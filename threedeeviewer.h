#pragma once

#include <QQuickItem>
#include <vtkSmartPointer.h>

class QQuickVTKRenderWindow;
class QQuickVTKRenderItem;

class ThreeDeeViewer : public QQuickItem {
  Q_OBJECT

  Q_PROPERTY(QQuickVTKRenderItem *renderer READ renderer WRITE setRenderer
                 NOTIFY rendererChanged)
  Q_PROPERTY(QQuickVTKRenderWindow *renderWindow READ renderWindow WRITE
                 setRenderWindow NOTIFY renderWindowChanged)
  Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

public:
  static void registerQML();

  explicit ThreeDeeViewer(QQuickItem *parent = nullptr);

  QQuickVTKRenderWindow *renderWindow() const;
  QQuickVTKRenderItem *renderer() const;
  QColor color() const;

  void setRenderer(QQuickVTKRenderItem *renderer);
  void setRenderWindow(QQuickVTKRenderWindow *renderWindow);
  void setColor(QColor color);

signals:
  void renderWindowChanged(QQuickVTKRenderWindow *renderWindow);
  void rendererChanged(QQuickVTKRenderItem *renderer);
  void colorChanged(QColor color);

private:
  QQuickVTKRenderWindow *m_renderWindow;
  QQuickVTKRenderItem *m_renderer;
  QColor m_color;
};