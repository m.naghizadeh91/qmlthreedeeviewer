import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.15

import VTK 9.1
import ThreeDeeViewer 1.0

ApplicationWindow 
{
  width: 1600
  height: 1200
  visible: true
  title: qsTr("3DViewer")

    ColumnLayout {
        anchors.fill: parent
        Viewer
        {
            Layout.fillWidth: true
            Layout.fillHeight: true
            id: viewer
            renderWindow.anchors.fill: viewer
            renderer.anchors.fill: viewer
            color: "red" // background color of renderer
        }

        //! Uncomment to see problem
        // Viewer {
        //     id: viewer2
        //     Layout.fillWidth: true
        //     Layout.fillHeight: true
        //     renderWindow.anchors.fill: viewer2
        //     renderer.anchors.fill: viewer2
        //     color: "blue"
        // }
        
        //! Uncomment to see problem
        // Viewer {
        //     id: viewer3
        //     Layout.fillWidth: true
        //     Layout.fillHeight: true
        //     renderWindow.anchors.fill: viewer3
        //     renderer.anchors.fill: viewer3
        //     color: "green"
        // }
    }
}